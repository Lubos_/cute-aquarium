# Cute Aquarium

## Description
We will create a creative project for pupils of art schools, how to present works of art in a modern way.
Children will draw fishes and water plants on paper.
They place their image on a prepared table where the image is photographed and converted into digital form.
Subsequently, a floating fish will appear in the digital aquarium (monitor/projector), or several fishes and aquatic plants.
The project will also include the creation of a table with a camera and lighting, maybe also with an integrated projector.

## Contributors
To be done

## Initial version authors and acknowledgment
* Lubos Mincak
* Mozenbakh Anton
* Muzhychuk Anastasiia
* Lysachenko Lev
* Kagirov Dmytro
* Kashytskyi Oleksandr

## License
Open source

## Project status
In progress

## Useful links
- [Qt - Moving object by path](https://www.youtube.com/watch?v=Ho73C_lFcrk)
- [Qt - Flipping object](https://www.youtube.com/watch?v=KNIySkOpce4)

## Imagination
- [Imagine 1](https://www.youtube.com/watch?v=Q0Heilbd7SI)
- [Imagine 2](https://www.youtube.com/watch?v=6O82W2OHU3o)
- [Imagine 3](https://www.youtube.com/watch?v=LKfe4rMSDTc)

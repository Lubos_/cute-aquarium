#include "configfile.h"
#include <QStandardPaths>
#include <fstream>
#include <QFile>
#include <QJsonDocument>

#define CONFIG_FNAME "config.json"

using namespace std;

ConfigFile::ConfigFile()
{
    // create QJsonObject with default parametrs
    this->json_obj["fishWidth"] = 150;
    this->json_obj["fishHeight"] = 100;
    this->json_obj["fishMinDuration"] = 10000;
    this->json_obj["fishMaxDuration"] = 20000;
    this->json_obj["fishDeltaWave"] = 100;
    this->json_obj["fishMaxScale"] = 1.8;
    this->json_obj["bubbleLifeSpan"] = 10000;
    this->json_obj["bubbleMainSize"] = 15;
    this->json_obj["fishTurnAngle"] = 30;
    this->json_obj["appDataPath"] = "./";
}

bool ConfigFile::loadAppConfig(){
    // get config path for current OS
    string configDirPath = QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation).toStdString();
    string configPath = configDirPath + "/" + CONFIG_FNAME;
    this->json_obj["appDataPath"] = QString(configDirPath.c_str());
    cout << "Config path: " << configPath << endl;

    // create configuration file if not exist with default parameters
    if(!ConfigFile::configExist(configPath)){
        bool created = ConfigFile::createConfigFile(configPath);
        if(!created){
            cout << "Cannot create " << CONFIG_FNAME << " file in " << configDirPath << endl;
            return false;
        }
    }

    return this->readJsonFile(configPath, this->json_obj);
}

std::ostream& operator<<(std::ostream& os, const ConfigFile& cf){
    // Log the loaded configuration
    os << "App configuration:" << endl;
    os << "  fishWidht: " << cf.json_obj["fishWidth"].toInt() << endl;
    os << "  fishHeight: " << cf.json_obj["fishHeight"].toInt() << endl;
    os << "  fishMinDuration: " << cf.json_obj["fishMinDuration"].toInt() << endl;
    os << "  fishMaxDuration: " << cf.json_obj["fishMaxDuration"].toInt() << endl;
    os << "  fishDeltaWave: " << cf.json_obj["fishDeltaWave"].toInt() << endl;
    os << "  bubbleLifeSpan: " << cf.json_obj["bubbleLifeSpan"].toInt() << endl;
    os << "  bubbleMainSize: " << cf.json_obj["bubbleMainSize"].toInt() << endl;
    os << "  fishTurnAngle: " << cf.json_obj["fishTurnAngle"].toInt() << endl;
    os << "  appDataPath: " << cf.json_obj["appDataPath"].toString().toStdString() << endl;
    return os;
}

bool ConfigFile::configExist(string name){
    ifstream f(name.c_str());
    return f.good();
}

bool ConfigFile::createConfigFile(string filePath){
    return this->writeJsonFile(this->json_obj, filePath);
}

bool ConfigFile::writeJsonFile(QJsonObject json_obj, string file_path){
    QJsonDocument json_doc(json_obj);
    QString json_string = json_doc.toJson();

    QFile save_file(QString::fromStdString(file_path));
    if (!save_file.open(QIODevice::WriteOnly)) {
        std::cout << "ConfigFile::writeJsonFile(): Failed to open save config file" << std::endl;
        return false;
    }
    save_file.write(json_string.toLocal8Bit());
    save_file.close();
    return true;
}

bool ConfigFile::readJsonFile(std::string file_path, QJsonObject& json_obj){
    // Load the file
    QFile file_obj(QString::fromStdString(file_path));
    if (!file_obj.open(QIODevice::ReadOnly)) {
        std::cout << "ConfigFile::readJsonFile(): Failed to open " << file_path << std::endl;
        return false;
    }

    // Convert file contents into a QbyteArray
    QTextStream file_text(&file_obj);
    QString json_string;
    json_string = file_text.readAll();
    file_obj.close();
    QByteArray json_bytes = json_string.toLocal8Bit();

    // Load it into a QJsonDocument
    auto json_doc = QJsonDocument::fromJson(json_bytes);

    if (json_doc.isNull()) {
        std::cout << "ConfigFile::readJsonFile(): Failed to create QJsonDocument." << std::endl;
        return false;
    }
    if (!json_doc.isObject()) {
        std::cout << "ConfigFile::readJsonFile(): json_doc is not an object." << std::endl;
        return false;
    }

    // Convert that to QJsonObject
    json_obj = json_doc.object();
    if (json_obj.isEmpty()) {
        std::cout << "ConfigFile::readJsonFile(): json_doc object is empty." << std::endl;
        return false;
    }
    return true;
}

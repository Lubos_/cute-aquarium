QT += quick

SOURCES += \
        configfile.cpp \
        main.cpp \
        scanFolders.cpp

# Sets the build directory
DESTDIR = ../CuteAquarium

# Copies assets folder to the project build directory and compile shader wobble.frag
win32 {
    system(if not exist "..\CuteAquarium" mkdir "..\CuteAquarium")
    system(qsb --glsl "100es,120,150" --hlsl 50 --msl 12 -o assets/shaders/wobble.frag.qsb assets/shaders/wobble.frag)
    system(if not exist "..\CuteAquarium\fish" Xcopy /E /I assets\fishes ..\CuteAquarium\fish)
    system(if not exist "..\CuteAquarium\background" Xcopy /E /I assets\background ..\CuteAquarium\background)
    system(if not exist "..\CuteAquarium\plant" Xcopy /E /I assets\plants ..\CuteAquarium\plant)
    system(if not exist "..\CuteAquarium\scripts" Xcopy /E /I ..\scripts ..\CuteAquarium\scripts)
}
unix{
    system(qsb --glsl "100es,120,150" --hlsl 50 --msl 12 -o assets/shaders/wobble.frag.qsb assets/shaders/wobble.frag)
    system(mkdir -p ../CuteAquarium)
    system(mkdir -p ../CuteAquarium/fish)
    system(mkdir -p ../CuteAquarium/background)
    system(mkdir -p ../CuteAquarium/plant)
    system(mkdir -p ../CuteAquarium/.fish)
    system(mkdir -p ../CuteAquarium/.background)
    system(mkdir -p ../CuteAquarium/.plant)
    system(mkdir -p ../CuteAquarium/.buffer)
    system(cp -a assets/fishes/. ../CuteAquarium/fish)
    system(cp -a assets/background/. ../CuteAquarium/background)
    system(cp -a assets/plants/. ../CuteAquarium/plant)
    system(cp -a ../scripts ../CuteAquarium/scripts)
}
macx {
    system(qsb --glsl "100es,120,150" --hlsl 50 --msl 12 -o assets/shaders/wobble.frag.qsb assets/shaders/wobble.frag)
    system(mkdir -p ../CuteAquarium)
    system(mkdir -p ../CuteAquarium/fish)
    system(mkdir -p ../CuteAquarium/background)
    system(mkdir -p ../CuteAquarium/plant)
    system(mkdir -p ../CuteAquarium/.fish)
    system(mkdir -p ../CuteAquarium/.background)
    system(mkdir -p ../CuteAquarium/.plant)
    system(mkdir -p ../CuteAquarium/.buffer)
    system(cp -a assets/fishes/. ../CuteAquarium/fish)
    system(cp -a assets/background/. ../CuteAquarium/background)
    system(cp -a assets/plants/. ../CuteAquarium/plant)
    system(cp -a ../scripts ../CuteAquarium/scripts)
}

resources.files = main.qml FishComponent.qml BubbleParticles.qml PlantComponent.qml \
    assets/bubbles/bubble.png assets/transparent.png assets/plants/plant_1.png \
    assets/shaders/wobble.frag.qsb
resources.prefix = /$${TARGET}
RESOURCES += resources \

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    BubbleParticles.qml \
    PlantComponent.qml \
    main.qml \
    FishComponent.qml

HEADERS += \
    configfile.h \
    scanFolders.h

#ifndef SCANFOLDERS_H
#define SCANFOLDERS_H

#include <QObject>
#include <vector>

using std::string, std::vector;

class ScanFolders : public QObject
{
    Q_OBJECT
public:
    explicit ScanFolders(QObject *parent = nullptr);

    // return all new fish images
    Q_INVOKABLE QList<QString> newFishes();

    // return all new fish images
    Q_INVOKABLE QList<QString> newPlants();

    // return last background image
    Q_INVOKABLE QString lastBackground();

    // return all removed fish images
    Q_INVOKABLE QList<QString> remFishes();

    // return all removed plant images
    Q_INVOKABLE QList<QString> remPlants();

    // return all removed background images
    Q_INVOKABLE QList<QString> remBg();

private:
    QList<QString> fishes;
    QList<QString> plants;
    QList<QString> backgrounds;
    int lastFishInd = -1;            // last fish index
    int lastPlantInd = -1;            // last fish index
    bool newBg = false;

signals:

};

#endif // SCANFOLDERS_H

#include "scanFolders.h"
#include <iostream>
#include <string>
#include <filesystem>

using std::cout, std::endl, std::filesystem::directory_iterator;

ScanFolders::ScanFolders(QObject *parent) : QObject{parent} { }

Q_INVOKABLE QList<QString> ScanFolders::newFishes(){
#ifdef Q_OS_DARWIN
    string fishesPath = "../../../fish/";
#else
    string fishesPath = "fish/";
#endif

    for(const auto &file : directory_iterator(fishesPath)){
        string fishPath = fishesPath.c_str() + file.path().filename().string();
        if(!this->fishes.contains(fishPath.c_str()))
            this->fishes.append(QString::fromLocal8Bit(fishPath.c_str()));
    }

    QList<QString> newFishesList = {};
    for(int i = this->lastFishInd+1; i < this->fishes.length(); i++)
        newFishesList.append(this->fishes[i]);

    this->lastFishInd = this->fishes.length() - 1;
    return newFishesList;
}

Q_INVOKABLE QList<QString> ScanFolders::newPlants(){
#ifdef Q_OS_DARWIN
    string plantsPath = "../../../plant/";
#else
    string plantsPath = "plant/";
#endif

    for(const auto &file : directory_iterator(plantsPath)){
        string plantPath = plantsPath.c_str() + file.path().filename().string();
        if(!this->plants.contains(plantPath.c_str()))
            this->plants.append(QString::fromLocal8Bit(plantPath.c_str()));
    }

    QList<QString> newPlantsList = {};
    for(int i = this->lastPlantInd+1; i < this->plants.length(); i++)
        newPlantsList.append(this->plants[i]);

    this->lastPlantInd = this->plants.length() - 1;
    return newPlantsList;
}

Q_INVOKABLE QString ScanFolders::lastBackground(){
#ifdef Q_OS_DARWIN
    string bgDirPath = "../../../background/";
#else
    string bgDirPath = "background/";
#endif

    for(const auto &file : directory_iterator(bgDirPath)){
        string bgPath = bgDirPath.c_str() + file.path().filename().string();
        if (!this->backgrounds.contains(bgPath.c_str())){
            this->backgrounds.append(QString::fromLocal8Bit(bgPath.c_str()));
            this->newBg = true;
        }
    }
    if(!this->backgrounds.isEmpty() && this->newBg){
        this->newBg = false;
        return this->backgrounds.last();
    }
    return NULL;
}

Q_INVOKABLE QList<QString> ScanFolders::remPlants(){
#ifdef Q_OS_DARWIN
    string plantsPath = "../../../plant/";
#else
    string plantsPath = "plant/";
#endif
    QList<QString> removedPlantsList = {};
    QList<QString> existingPlantsList = {};
    for(const auto &file : directory_iterator(plantsPath)){
        string plantPath = plantsPath.c_str() + file.path().filename().string();
        existingPlantsList.append(plantPath.c_str());
    }
    for (int i=0; i<this->plants.size(); i++) {
        if(!existingPlantsList.contains(this->plants[i])){
            removedPlantsList.append(this->plants[i]);
            this->plants.remove(i, 1);
            this->lastPlantInd--;
        }
    }
    return removedPlantsList;
}

Q_INVOKABLE QList<QString> ScanFolders::remFishes(){
#ifdef Q_OS_DARWIN
    string fishesPath = "../../../fish/";
#else
    string fishesPath = "fish/";
#endif
    QList<QString> removedFishesList = {};
    QList<QString> existingFishesList = {};
    for(const auto &file : directory_iterator(fishesPath)){
        string fishPath = fishesPath.c_str() + file.path().filename().string();
        existingFishesList.append(fishPath.c_str());
    }
    for (int i=0; i<this->fishes.size(); i++) {
        if(!existingFishesList.contains(this->fishes[i])){
            removedFishesList.append(this->fishes[i]);
            this->fishes.remove(i, 1);
            this->lastFishInd--;
        }
    }
    return removedFishesList;
}

Q_INVOKABLE QList<QString> ScanFolders::remBg(){
#ifdef Q_OS_DARWIN
    string backgoundsPath = "../../../background/";
#else
    string backgoundsPath = "background/";
#endif
    QList<QString> removedBgList = {};
    QList<QString> existingBgList = {};
    for(const auto &file : directory_iterator(backgoundsPath)){
        string bgPath = backgoundsPath.c_str() + file.path().filename().string();
        existingBgList.append(bgPath.c_str());
    }
    for (int i=0; i<this->backgrounds.size(); i++) {
        if(!existingBgList.contains(this->backgrounds[i])){
            removedBgList.append(this->backgrounds[i]);
            this->backgrounds.remove(i, 1);
            this->newBg = true;
        }
    }
    return removedBgList;
}

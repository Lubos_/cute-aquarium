#ifndef CONFIGFILE_H
#define CONFIGFILE_H

#include <iostream>
#include <QVariant>
#include <QJsonObject>

typedef unsigned int uint;

class ConfigFile
{
public:
    // configuration parameters in QJsonObject type
    QJsonObject json_obj;

    ConfigFile();

    // loads data from config file if it exists
    // otherwise creates it with default parameters
    bool loadAppConfig();

    // override the << operator to print object by:
    // cout << configFileObject
    friend std::ostream& operator<<(std::ostream& os, const ConfigFile& cf);

private:
    // if app.config exist return true else false
    bool configExist(std::string name);

    // create configuration file with default parameters
    bool createConfigFile(std::string filePath);

    // write a QVariantMap to disk like json file
    bool writeJsonFile(QJsonObject json_obj, std::string file_path);

    // read a json file from disk to QVariantMap
    bool readJsonFile(std::string file_path, QJsonObject& json_obj);
};

#endif // CONFIGFILE_H

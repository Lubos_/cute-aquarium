#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlComponent>
#include <QQmlContext>
#include <QDir>
#include <QQuickWindow>
#include <QProcess>
#include <iostream>
#include "configfile.h"
#include "scanFolders.h"

using namespace std;

int main(int argc, char *argv[])
{
    cout << "Start application" << endl;

    // register my own cpp type
    qmlRegisterType<ScanFolders>("ScanFolders", 1, 0, "ScanFolders");

    // TODO: automatical starting of a python script
    // // Create a QProcess instance
    // QProcess process;
    // // Set the program and arguments
    // process.setProgram("python3");
    // QStringList arguments;
    // arguments << "./scripts/buttons.py";
    // process.setArguments(arguments);

    // // Check if the process started successfully
    // if (process.waitForStarted()) {
    //    cout << "buttons.py started successfully" << endl;
    // } else {
    //     std::cerr << "Error starting buttons.py\n";
    //     return 1;
    // }

    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("currDirPath", QString(QDir::currentPath()));
    const QUrl url("qrc:/CuteAquarium/main.qml");
    QQmlComponent component(&engine, url);

    // Check for errors
    if (component.status() == component.Error)
        cout << component.errors()[0].toString().toStdString() << endl;

    // Initialize root window
    QObject *window = component.create();

    // Set window state to fullscreen
//    if (window)
//    {
//        QQuickWindow *quickWindow = qobject_cast<QQuickWindow *>(window);
//        if (quickWindow)
//        {
//            quickWindow->showFullScreen();
//        }
//    }

    // Load the configuration parameters from config file
    ConfigFile confFile = ConfigFile();
    confFile.loadAppConfig();
    cout << confFile << endl;

    // Set configuration in qt application
    QMetaObject::invokeMethod(window, "setConfiguration", Q_ARG(QVariant, confFile.json_obj));

    return app.exec();
}

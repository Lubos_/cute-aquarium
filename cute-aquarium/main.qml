import QtCore
import QtQuick
import QtQuick.Controls
import QtQuick.Dialogs
import QtQuick.Particles
import ScanFolders

Window {
    id: window
    visible: true
    title: qsTr("Cute Aquarium")
    Component.onCompleted: {
        //window.showFullScreen();  // Opens the window to full screen
        //window.showMaximized();
        console.log("Current build path: " + currDirPath)
    }
    width: 800
    height: 600

    property var items: ({})        // all components: fishes and plants
    property var config: ({bubbleMainSize: 0, bubbleLifeSpan: 0})       // Configuration object with some default parameters

    // Sets the background for the aquarium. Takes image as argument
    function setBackground(selectedFile){
        backgroundImage.source = selectedFile
        closeHandle.focus = true
    }

    // Add plant to the aquarium. Takes image as argument
    function addPlant(selectedFile){
        var plantComponent = Qt.createComponent("PlantComponent.qml")
        var plantObject = undefined
        if (plantComponent.status === Component.Ready) {
            plantObject = plantComponent.createObject(window)
            plantObject.source = selectedFile
            items[selectedFile] = plantObject
        }
        // Error Handling
        if (plantComponent === null)
            console.log("Error creating plant object");

        closeHandle.focus = true
        return plantObject
    }

    // Add fish to the aquarium. Takes image as argument
    function addFish(selectedFile) {
        var fishComponent = Qt.createComponent("FishComponent.qml")
        var fishObject = undefined
        if (fishComponent.status === Component.Ready) {
            fishObject = fishComponent.createObject(window)
            fishObject.imgSource = selectedFile
            items[selectedFile] = fishObject
        }
        // Error Handling
        if (fishComponent === null)
            console.log("Error creating fish object");

        closeHandle.focus = true
        return fishObject
    }

    // This function remove component from aquarium
    function removeComponent(selectedFile) {
        if(items[selectedFile]){
            items[selectedFile].destroy()
        }
    }

    // This method is called from the main.cpp and save the configuration settings
    // from the config file to the this.config object
    function setConfiguration(json_obj) {
        config = json_obj
    }

    // Checks the fish folder and the background folder every second.
    // If new fish appear, then add them to the aquarium
    ScanFolders { id: scanFolders; }
    Timer {
        interval: 1000; running: true; repeat: true
        onTriggered: {
            for(const fish of scanFolders.newFishes()){
                window.addFish("file:///" + currDirPath + "/" + fish);
                console.log("New fish: " + fish)
            }
            let bg = scanFolders.lastBackground()
            if(bg !== ""){
                window.setBackground("file:///" + currDirPath + "/" + bg);
                console.log("New background: " + bg)
            }
            for(const plant of scanFolders.newPlants()){
                window.addPlant("file:///" + currDirPath + "/" + plant);
                console.log("New plant: " + plant)
            }
            for(const remFish of scanFolders.remFishes()){
                console.log("Removed fishe: " + remFish)
                window.removeComponent("file:///" + currDirPath + "/" + remFish)
            }
            for(const remPlant of scanFolders.remPlants()){
                console.log("Removed plant: " + remPlant)
                window.removeComponent("file:///" + currDirPath + "/" + remPlant)
            }
            for(const remBg of scanFolders.remBg()){
                console.log("Removed bg: " + remBg)
                if ("file:///" + currDirPath + "/" + remBg == backgroundImage.source){
                    bg = scanFolders.lastBackground()
                    if(bg !== ""){
                        window.setBackground("file:///" + currDirPath + "/" + bg);
                        console.log("New background: " + bg)
                    }
                    else {
                        backgroundImage.source = '';
                    }
                }
            }
        }
    }

    Timer {
      id: startTimer

      interval: 3000; running: true; repeat: false
      onTriggered: {
        window.showFullScreen()
      }
    }

    // Pressing the Esc key close the window
    Item {
        id: closeHandle
        focus: true
        Keys.onEscapePressed: window.close()

        Keys.onPressed: (event)=> {
            if (event.key === Qt.Key_M) {
                curtain.leftSliderVisible = !curtain.leftSliderVisible
                curtain.rightSliderVisible = !curtain.rightSliderVisible
                event.accepted = true;
            }
        }
        Keys.onRightPressed: if(curtain.leftSliderValue <= window.width - 10) curtain.leftSliderValue += 10
        Keys.onLeftPressed: if(curtain.leftSliderValue >= 10) curtain.leftSliderValue -= 10
        Keys.onUpPressed: if(curtain.rightSliderValue <= window.width - 10) curtain.rightSliderValue += 10
        Keys.onDownPressed: if(curtain.rightSliderValue >= 10) curtain.rightSliderValue -= 10
    }

    BubbleParticles { z: 1; xPos: window.width - window.width/5 }
    BubbleParticles { z: 1; xPos: window.width - (window.width*3)/5 }

    Image {
        id: backgroundImage
        z: -1
        anchors.fill: parent
        fillMode: Image.PreserveAspectCrop
    }

    Item {
        id: curtain

        property alias leftSliderValue: leftSlider.value
        property alias rightSliderValue: rightSlider.value
        property alias leftSliderVisible: leftSlider.visible
        property alias rightSliderVisible: rightSlider.visible

        anchors.fill: parent
        z: 100 // see fishMaxScale

        Slider {
          id: leftSlider

          x: 0; y: curtain.height / 2; width: curtain.width / 2;
          from: 0; to: curtain.width / 2; value: 0
          visible: false
        }

        Slider {
          id: rightSlider

          x: curtain.width / 2; y: curtain.height / 2; width: curtain.width / 2;
          from: curtain.width / 2; to: 0; value: 0
          visible: false
        }

        Rectangle {
          x: 0; y: 0; width: leftSlider.value; height: curtain.height
          color: "black"
        }

        Rectangle {
          x: curtain.width - rightSlider.value; y: 0; width: curtain.width / 2; height: curtain.height
          color: "black"
        }
    }
}

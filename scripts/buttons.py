#!/usr/bin/env python
import enum
import multiprocessing
from threading import Thread
from gpiozero import Button
from signal import pause
import datetime
import yaml
import os
import glob
from PIL import Image
from loguru import logger
from pathlib import Path
from shutil import move


import getpass
import pathlib
# print( f'Path: { Path.home()} !' )
# print("User:" + getpass.getuser())
# print(os.path.abspath(os.getcwd())) #gives root because of running as sudo


'''buttons group layout > background_elements, background, fish'''

config = None
class ImageType(str, enum.Enum):
    background = "background"
    fish = "fish"
    background_element = "background_element"

class ConfigError(Exception):
    pass

'''
TODO:
- config file checking
    - yaml schema checking
- make more error output in logs
- remake home path into dynamic in a config file
'''


def configured_now() -> str:
    return str(datetime.datetime.now().strftime("%Y_%m_%d_%H:%M:%S"))


'''gets current log file name in *args'''
def clean_old_logs(*args) -> None:
    logs_file_number = 0
    for path in os.scandir(os.path.expanduser(config.get('logs_path'))):
        if path.is_file() and path.name.startswith(config.get('log_file_prefix')) and path.name.endswith('.log'):
            logs_file_number += 1
    if logs_file_number <= int(config.get('maximal_log_file_number')):
        return

    # remove the oldest file
    # sorting file list based on the creation time of the files
    logs_file_list = list(filter(lambda file_name: file_name.startswith(config.get('log_file_prefix')) and file_name.endswith('.log'), os.listdir(os.path.expanduser(config.get('logs_path')))))
    logs_file_list.sort(key=lambda x: os.path.getctime(os.path.expanduser(config.get('logs_path')) + x), reverse=False)

    # remove all extra files
    files_number_to_delete = logs_file_number - int(config.get('maximal_log_file_number'))
    for file_to_del in range(0, files_number_to_delete):
        os.remove(os.path.expanduser(config.get('logs_path')) + logs_file_list[file_to_del])
        logger.info(f'Removed old log file: {logs_file_list[file_to_del]}')


def get_config( file:  str | None=None):
    global config

    if config is not None:
        return config
    try:
        with open(file, 'r') as config_file:
            logger.info(f'Config file: {file} was successfully read!')
            return yaml.safe_load(config_file)
    except Exception as e:
        logger.error('Problems with configuration file: ' + e)
        raise ConfigError


def get_scan_command(image_type: ImageType) -> str:
    scan_path = None
    match image_type:
        case ImageType.background:
            scan_path = config.get('background_scan_path') + config.get('background_file_prefix')
        case ImageType.background_element:
            scan_path = config.get('background_elem_scan_path') + config.get('background_elem_file_prefix')
        case ImageType.fish:
            scan_path = config.get('fish_scan_path') + config.get('fish_file_prefix')
        case _:
            logger.error("Unknown image type")

    return '--device-name=' + config.get('device-name') + \
           ' --resolution=' + config.get('resolution') + \
           ' --format=' + config.get('format') + \
           ' --output-file=' + os.path.expanduser(scan_path) + \
            configured_now() + '.' + config.get('format')


def scan_background():
    logger.info('scan_background scanimage ' + get_scan_command(ImageType.background))
    os.system('scanimage ' + get_scan_command(ImageType.background))


def scan_background_element():
    logger.info('scan_background_element scanimage ' + get_scan_command(ImageType.background_element))
    os.system('scanimage ' + get_scan_command(ImageType.background_element))


def scan_fish():
    logger.info('scan_fish scanimage ' + get_scan_command(ImageType.fish))
    os.system('scanimage ' + get_scan_command(ImageType.fish))


def delete_last_scan(button):
    logger.info('called')
    match button:
        case Button(pin=button.pin) if str(button.pin) == str(config.get('delete_last_scan_fish_pin')):
            delete_object_path = config.get('fish_processed_path')
            rename_object_path = config.get('fish_scan_path')
        case Button(pin=button.pin) if str(button.pin) == str(config.get('delete_last_scan_background_pin')):
            delete_object_path = config.get('background_processed_path')
            rename_object_path = config.get('background_scan_path')
        case Button(pin=button.pin) if str(button.pin) == str(config.get('delete_last_scan_background_elem_pin')):
            delete_object_path = config.get('background_elem_processed_path')
            rename_object_path = config.get('background_elem_scan_path')
        case _:
            logger.info(f'Unknown button pin: {button.pin}')
            return

    delete_object_path = os.path.join(os.path.expanduser(delete_object_path), "*")

    aimed_images = list(filter(os.path.isfile, glob.glob(delete_object_path)))

    aimed_images.sort(key=lambda x: os.path.getctime(x), reverse=True)
    image_to_delete = aimed_images[0] if len(aimed_images) > 0 else None
    if image_to_delete is None:
        return

    os.remove(image_to_delete)
    logger.info(f'Deleted file: {image_to_delete}')

    rename_object_path = os.path.join(os.path.expanduser(rename_object_path),'*')
    images_to_rename = list(filter(os.path.isfile, glob.glob(rename_object_path)))

    # cut ending of file name and start of absolute path
    image_to_del_main_name = image_to_delete.rstrip(f"{config.get('processed_file_ending')}.{config.get('format')}").rsplit('/', 1)[-1]
    for image_to_rename in images_to_rename:
        if str(image_to_del_main_name) in str(image_to_rename):
            image_to_rename_no_format = image_to_rename.rstrip(f".{config.get('format')}")
            os.rename(image_to_rename,f'{image_to_rename_no_format}{config.get("removed_file_prefix")}.{config.get("format")}')


def start_fish_watcher():
    # TODO: uncomment
    # os.system('python3 {}{}'.format(os.path.expanduser(config.get('application_path')), config.get('fish_script_name')))
    os.system('python3 {}{}'.format('/home/adminrpi/Desktop/CuteAquarium/scripts/', config.get('fish_script_name')))


def start_background_watcher():
    # TODO: uncomment
    # os.system('python3 {}{}'.format(os.path.expanduser(config.get('application_path')), config.get('background_script_name')))
    os.system('python3 {}{}'.format('/home/adminrpi/Desktop/CuteAquarium/scripts/', config.get('background_script_name')))


def start_frontend():
    process = multiprocessing.Process(target = lambda: os.system(os.path.expanduser(config.get('frontend_file_path'))))
    process.start()


def buffer_manager():
    while True:
        files = os.listdir(os.path.expanduser(config.get('image_buffer_path')))
        files = list(map(lambda file_: os.path.join(os.path.expanduser(config.get('image_buffer_path')), file_), files))
        for file in files:
            # check if image is empty
            if not Path(file).exists() or not Path(file).is_file() or not os.stat(file).st_size:
                continue
            try:
                img = Image.open(file)
                img.verify()
            except Exception:
                continue

            move_path = None
            # order if conditions is important
            if config.get('background_elem_file_prefix') in file:
                move_path = config.get('background_elem_processed_path')
            elif config.get('background_file_prefix') in file:
                move_path = config.get('background_processed_path')
            elif config.get('fish_file_prefix') in file:
                move_path = config.get('fish_processed_path')
            else:
                logger.error('Unknown file prefix!')
                continue

            move_path = os.path.expanduser(move_path)

            final_dir_files = os.listdir(move_path)
            move_path_files = list(map(lambda file_path: os.path.join(move_path,file_path), final_dir_files))

            # if file with same name is located in resulting directory, it will be deleted and replaced with one from .buffer
            for move_path_file in move_path_files:
                if str(file.rsplit('/', 1)[-1]) in move_path_file:
                    os.remove(move_path_file)

            move(file, move_path)
            logger.info(f'Moved file: {file} to path: {move_path}')


def start_watchers():
    fish_process = multiprocessing.Process(target=start_fish_watcher)
    fish_process.start()

    buffer_process = multiprocessing.Process(target=buffer_manager)
    buffer_process.start()

    background_thread = Thread(target=start_background_watcher)
    background_thread.start()



def main():
    # start scripts
    start_watchers()

    background_delete_last_button = Button(config.get('delete_last_scan_background_pin'))
    background_scan_button = Button(config.get('background_scan_button_pin'))

    background_element_delete_last_button = Button(config.get('delete_last_scan_background_elem_pin'))
    background_element_scan_button = Button(config.get('background_element_scan_pin'))

    fish_delete_last_button = Button(config.get('delete_last_scan_fish_pin'))
    fish_scan_button = Button(config.get('fish_scan_button_pin'))

    print("Press the right button...!")
    background_delete_last_button.when_pressed = delete_last_scan
    background_scan_button.when_pressed = scan_background

    background_element_delete_last_button.when_pressed = delete_last_scan
    background_element_scan_button.when_pressed = scan_background_element

    fish_delete_last_button.when_pressed = delete_last_scan
    fish_scan_button.when_pressed = scan_fish
    pause()



if __name__ == '__main__':
    try:
        config = get_config('./config.yml')
    except ConfigError:
        exit(1)

    # TODO: uncomment to turn in logging
    # must be here
    # log cleaner starts when program is finished not by itself
    logger.add(os.path.join(os.path.expanduser(config.get('logs_path')), f'{config.get("log_file_prefix")}{configured_now()}.log'),
               level="TRACE",
               format="<green>{time:DD:MM:YYYY at hh:mm:ss}</green>  | <level>{level}</level> |"
                      " <cyan>{name}</cyan>:<cyan>{function}</cyan>:<cyan>{line}</cyan> - <level>{message}</level>",
               retention=clean_old_logs,
               backtrace=True, diagnose=True)
    logger.info("Program started by user: " + os.getlogin())

    main()
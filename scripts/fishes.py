# pip install Pillow
# https://pillow.readthedocs.io/en/stable/reference/index.html

from PIL import Image
import os
from buttons import get_config, logger
from image_processing import saveImage, savePath, selectObject, findGreatest


config = None
try:
   config = get_config('config.yml')
except Exception as e:
   exit(1)


def resizeImage(imageObject, imageData):
    width, height = imageObject.size
    firstX = width - 1
    lastX = 0
    firstY = height - 1
    lastY = 0

    for index, value in enumerate(imageData):
        if value[-1] != 0:
            Xindex = index % width
            Yindex = int((index + 1) / width)
            if Xindex < firstX:
                firstX = Xindex
            if Xindex > lastX:
                lastX = Xindex
            if Yindex < firstY:
                firstY = Yindex
            if Yindex > lastY:
                lastY = Yindex

    imageObject = imageObject.crop((firstX - config.get('fish_processing_margin')['right'],
                                    firstY - config.get('fish_processing_margin')['bottom'],
                                    config.get('fish_processing_margin')['left'] + lastX,
                                    config.get('fish_processing_margin')['top'] + lastY))
    return imageObject


def convertImage(imageName, imagePath, saveDirectory):
    logger.info(f"Started conversion process for {imagePath} {imageName}, wait...")
    imageObject = Image.open(imagePath).convert('RGBA')
    imageObject = selectObject(imageObject, 90)
    imageObject = findGreatest(imageObject)
    imageData = imageObject.getdata()
    imageObject = resizeImage(imageObject, imageData)
    imageData = imageObject.getdata()

    saveImage(imageName, saveDirectory, imageObject, imageData, config)
    logger.info(f"Conversion process is done for {imagePath} {imageName}")


def main():
    processing_files = []
    while True:
        files = os.listdir(os.path.expanduser(config.get('fish_scan_path')))
        for file in files:
            try:
                fileName, fileType = file.split('.')
                if not os.path.exists(savePath(os.path.expanduser(config.get("fish_processed_path")), fileName, config)) and \
                        fileType == config.get('format') and \
                        not fileName.endswith(config.get('removed_file_prefix')) and \
                        not (os.stat(f'{os.path.expanduser(config.get("fish_scan_path"))}{file}').st_size == 0) and \
                        file not in processing_files:
                    # print(file)
                    img = Image.open(f'{os.path.expanduser(config.get("fish_scan_path"))}{file}')
                    try:
                        img.verify()
                        processing_files.append(file)
                        convertImage(fileName, f'{os.path.expanduser(config.get("fish_scan_path"))}{file}', os.path.expanduser(config.get('image_buffer_path')))
                        processing_files.remove(file)
                    except Exception:
                        pass
            except ValueError as value_error:
                logger.error(f'file: {file}, error reason: {str(value_error)}')



if __name__ == '__main__':
    main()

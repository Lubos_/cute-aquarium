# pip install Pillow
# https://pillow.readthedocs.io/en/stable/reference/index.html
import multiprocessing

from PIL import Image, ImageFile
from image_processing import saveImage, savePath, selectObject, findGreatest
import os
from buttons import ImageType, get_config, ConfigError, logger

ImageFile.LOAD_TRUNCATED_IMAGES = True

# TODO: normal paths, logs, exceptions

config = None
try:
   config = get_config('config.yml')
except Exception as e:
   exit(1)

processing_files = []



# remove white strip
def resizeImage(imageObject):
    width, height = imageObject.size
    imageObject = imageObject.crop((0, 0, width - 105, height))
    return imageObject


# main process
def convertImage(imageName, imagePath, saveDirectory, imageType: ImageType):
    logger.info(f"Started conversion process for {imagePath} {imageName}, wait...")

    imageObject = Image.open(imagePath).convert('RGBA')
    imageObject = resizeImage(imageObject)

    if imageType == ImageType.background_element:
        imageObject_new = selectObject(imageObject, 90)
        imageObject = findGreatest(imageObject_new)

    imageData = imageObject.getdata()
    saveImage(imageName, saveDirectory, imageObject, imageData, config)
    logger.info(f"Conversion process is done for {imagePath} {imageName}")


def folderLookUp(sources_folder, saves_folder, buffer_folder, imageType: ImageType):
        files = os.listdir(sources_folder)
        for file in files:
            try:
                fileName, fileType = file.split('.')

                if not os.path.exists(savePath(saves_folder, fileName, config)) and \
                        fileType == config.get('format') and \
                        not fileName.endswith(config.get('removed_file_prefix')) and \
                        not (os.stat(f'{sources_folder}{file}').st_size == 0) and \
                        file not in processing_files:
                    img = Image.open(f'{sources_folder}{file}')
                    try:
                        img.verify()
                        processing_files.append(file)
                        convertImage(fileName, f'{sources_folder}{file}', buffer_folder, imageType)
                        processing_files.remove(file)
                    except Exception:
                        pass
            except ValueError as value_error:
                logger.error(f'file: {file}, error reason: {str(value_error)}')

def start_background_loop():
    while True:
        folderLookUp(os.path.expanduser(config.get('background_scan_path')),
                     os.path.expanduser(config.get('background_processed_path')),
                     os.path.expanduser(config.get('image_buffer_path')),
                     ImageType.background)

def start_background_elem_loop():
    while True:
        folderLookUp(os.path.expanduser(config.get('background_elem_scan_path')),
                     os.path.expanduser(config.get('background_elem_processed_path')),
                     os.path.expanduser(config.get('image_buffer_path')),
                     ImageType.background_element)



def main():
    global config
    try:
        config = get_config('config.yml')
    except ConfigError:
        exit(1)

    background_process = multiprocessing.Process(target=start_background_loop)
    background_process.start()

    background_elem_process = multiprocessing.Process(target=start_background_elem_loop)
    background_elem_process.start()


if __name__ == '__main__':
    main()
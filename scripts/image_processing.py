import numpy as np
from PIL import Image, ImageDraw, ImageOps
import cv2
from copy import deepcopy


savePath = lambda saveDirectory, fileName, config: f'{saveDirectory}{fileName}{config.get("processed_file_ending")}.{config.get("format")}'


# remove white color
def selectObject(imageObject, tresh_parameter):
    imageObject = ImageOps.expand(imageObject, border=50, fill=(255, 255, 255, 255))
    ImageDraw.floodfill(imageObject, (0, 0), (255, 255, 255, 0), thresh=tresh_parameter)
    return imageObject


# finds the greatest element
def findGreatest(imageObject):
    width, height = imageObject.size
    center = (int(width/2), int(height/2))
    clone = deepcopy(imageObject)
    imageData = imageObject.getdata()

    data = []
    for pixel in clone.getdata():
        if pixel[-1] != 0:
            data.append((0, 0, 0, 255))
        else:
            data.append((255, 255, 255, 0))

    clone.putdata(data)

    # convert image to opencv format
    numpy_image = np.array(clone)
    opencv_image = cv2.cvtColor(numpy_image, cv2.COLOR_RGB2BGR)
    center = find_center(opencv_image)

    print(center)

    ImageDraw.floodfill(clone, center, (123, 123, 123, 123), thresh=20)

    controlData = clone.getdata()
    newData = []

    for index, value in enumerate(controlData):
        if value == (123, 123, 123, 123):
            newData.append(imageData[index])
        else:
            newData.append((255, 255, 255, 0))
    imageObject.putdata(newData)
    return imageObject


def find_center(img):
    # median blur
    median = cv2.medianBlur(img, 5)

    # threshold on black
    lower = (0, 0, 0)
    upper = (0, 0, 0)
    thresh = cv2.inRange(median, lower, upper)

    # apply morphology open and close
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
    morph = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel)
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (29, 29))
    morph = cv2.morphologyEx(morph, cv2.MORPH_CLOSE, kernel)

    # filter contours on area
    contours = cv2.findContours(morph, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    contours = contours[0] if len(contours) == 2 else contours[1]
    result = img.copy()
    for c in contours:
        area = cv2.contourArea(c)
        if area > 1000:
            cv2.drawContours(result, [c], -1, (0, 0, 255), 2)


    sorted_contours = sorted(contours, key=cv2.contourArea, reverse=True)
    largest_item = sorted_contours[0]

    M = cv2.moments(sorted_contours[0])
    cX = int(M["m10"] / M["m00"])
    cY = int(M["m01"] / M["m00"])
    return cX, cY


# if image was scanned nor in landscape, it will rotate it to landscape position
def rotate_image(image: Image) -> Image:
    if image.width < image.height:
        # rotate inner image including canvas
        return image.transpose(Image.ROTATE_90)
    else:
        return image

def saveImage(imageName, saveDirectory, imageObject, imageData, config):
    imageObject.putdata(imageData)
    newImageObject = rotate_image(imageObject)
    newImageObject.save(savePath(saveDirectory, imageName, config), config.get("format"))